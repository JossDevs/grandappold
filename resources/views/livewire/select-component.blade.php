<head>
@livewireStyles

</head>
<body>
    <div class="form-group">
    <label for="email" class="col-sm-2 control-label">
        Provincia</label>
    <div class="col-sm-10">
        <div class="row">
            <div class="col-sm-10">
                {{$provinces}}
                <select  wire:model="selectedProvince">
                    <option>_Seleccione_Provinci</option>
                    @foreach($provinces as $province)
                        <option value="{{$province->id_provinces}}">{{$province->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>


@livewireScripts
</body>
</html>

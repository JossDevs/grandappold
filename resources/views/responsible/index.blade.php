@extends('adminlte::page')

@section('title', 'Servicios')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Responsables/Trabajo en Equipo</b></h3>
@stop

@section('content')


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>

    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Ingrese Responsable o Equipo</b></h5>

        <form action="responsible" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="type" name="type">
                                    <option>_Seleccione Tipo Em</option>
                                    <option value="Responsable">Responsable</option>
                                    <option value="Equipo">Equipo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="name" name="name" placeholder="Nombre" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Corto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="short_name" name="short_name" placeholder="N_Corto" />
                    </div>
                </div>


                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Estado</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="status" name="status">
                                    <option>_Seleccione Estado</option>
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                            Fecha_Inicio</label>
                        <div class="col-sm-12">
                            <input class="form-control-sm" type="date" id="start_date" name="start_date"
                                   required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                            Fecha_Fin</label>
                        <div class="col-sm-12">
                            <input class="form-control-sm" type="date" id="finish_date" name="finish_date"
                                   required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                            Comentario</label>
                        <div class="col-md-14">
                            <textarea type="text" class="form-control" id="comments" name="comments" rows="2"  cols="40" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="logo" class="col-sm-2 control-label">
                            Miembros</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-sm" id="members" name="members" accept="image/*" />
                            @error('file')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-sm">
                            Guardar Registro</button>

                    </div>


                </div>


        </form>


        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">


                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Listado_Productos</b></h2>
                        </div>


                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Nombre</th>
                        <th>Nombre_Corto</th>
                        <th>Comentarios</th>
                        <th>Estado</th>
                        <th>Fecha_Inicio</th>
                        <th>Fecha_Final</th>
                        <th>Miembros</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($responsibles as $responsible)
                        <tr>
                            <td>{{$responsible->type}}</td>
                            <td>{{$responsible->name}}</td>
                            <td>{{$responsible->short_name}}</td>
                            <td>{{$responsible->comments}}</td>
                            <td>{{$responsible->status}}</td>
                            <td>{{$responsible->start_date}}</td>
                            <td>{{$responsible->finish_date}}</td>
                            <td>
                                <img src="{{asset($responsible->members)}}" alt="{{$responsible->title}}" class="img-fluid" width="40px" height=="40px" style="border-radius:20px" >
                            </td>
                            <td>
                                <a href="#" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                <a href="#" class="delete" title="Eliminar" data-toggle="tooltip"><i class="fas fa-trash-alt">&#xE5C9;</i></a>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    </body>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop


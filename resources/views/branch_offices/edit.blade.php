@extends('adminlte::page')

@section('title', 'Sucursales')

@section('content_header')
    <h1 style="color:#5D676F;"><b>Administrador Sucursales</b></h1>
@stop

@section('content')
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap User Management Data Table</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>




    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Gestión Sucursales</b></h5>




        <form action="{{route('branch.update',$branch->id_branches)}}" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <input type="hidden" id="registers_id" name="registers_id" value="{{$branch->registers_id}}"  />

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo_</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="type" name="type" value="{{$branch->type}}">
                                    <option>_Seleccione / Tipo_S</option>
                                    <option value="Matriz ">Matriz</option>
                                    <option value="Sucursal ">Sucursal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N_Sucursal</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="branch" name="branch" placeholder="Sucursal" value="{{$branch->branch}}" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Comercial</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="name" name="name" placeholder="Nombre Comercial" value="{{$branch->name}}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        País</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="country" name="country" value="{{$branch->country}}" >
                                    <option>_Seleccione_Provincia</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id_countries}}">{{$country->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Provincia</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="province" name="province" value="{{$branch->province}}">
                                    <option>_Seleccione_Provincia</option>
                                    @foreach($provinces as $province)
                                        <option value="{{$province->id_provinces}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Provincia</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="city" name="city" value="{{$branch->city}}">
                                    <option>_Seleccione_Provincia</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id_cities}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Teléfono</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="telephone"  name="telephone" placeholder="Teléfono" value="{{$branch->telephone}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Celular</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="phone"  name="phone" placeholder="Celular"  value="{{$branch->phone}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Dirección</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="direction" name="direction" placeholder="Dirección" value="{{$branch->direction}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Correo_Electrónico</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control-sm" id="email" name="email" placeholder="Correo" value="{{$branch->email}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Sítio_Web</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="website" name="website" placeholder="Sítio_Web" value="{{$branch->website}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Redes_Sociales</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="social_media" name="social_media" placeholder="Redes Sociales" value="{{$branch->social_media}}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Inicio</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="start_date" name="start_date"
                               required  value="{{$branch->start_date}}" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Final</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="final_date" name="final_date"
                               required  value="{{$branch->final_date}}" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Actividad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="activity" name="activity" placeholder="Actividad" value="{{$branch->activity}}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="logo" class="col-sm-2 control-label">
                        Logo</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control-sm" id="logo" name="logo" accept="image/*" value="{{$branch->logo}}" />
                        @error('file')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="firm" class="col-sm-2 control-label">
                        Firma</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control-sm" id="firm" name="firm" accept="image/*" value="{{$branch->firm}}" />
                        @error('file')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Guardar Registro</button>

                </div>


            </div>


        </form>




    </div>

    </body>

    <script>
        public function nuevaEmpresa(){
            $('#myModal').modal('show');
            // www.jquery2dotnet.com

        }

    </script>

    @stop

    @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
        <script> console.log('Hi!'); </script>
@stop

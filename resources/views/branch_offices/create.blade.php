@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="color:#5D676F;  font-family: 'Varela Round', sans-serif;" ><b>Registro Empresa</b></h1>
@stop

@section('content')
    <form class="row g-3" action="/register" method="POST">
       @csrf
        <div class="col-md-12">
            <label for="email" class="col-sm-2 control-label" style="color:#008CFE;">
                Ubicación</label>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-md-6">
                        <select class="form-control">
                            <option>Seleccione</option>
                            <option>Matríz</option>
                            <option>Sucursal</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="inputEmail4" class="form-label" style="color:#008CFE;">Razón Social</label>
            <input type="text" class="form-control" id="person_type" name="person_type">
        </div>
        <div class="col-md-6">
            <label for="inputPassword4" class="form-label" style="color:#008CFE;">Nombre Comercial</label>
            <input type="text" class="form-control" id="tradename" name="tradename">
        </div>
        <div class="col-md-6">
            <label for="inputAddress" class="form-label" style="color:#008CFE;">Cédula o Ruc</label>
            <input type="number" class="form-control" id="id_ruc" name="id_ruc" placeholder="">
        </div>
        <div class="col-md-6">
            <label for="inputAddress2" class="form-label" style="color:#008CFE;">Teléfono Convencional</label>
            <input type="number" class="form-control" id="telephone" name="telephone" placeholder="">
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Celular</label>
            <input type="number" class="form-control" id="mobile" name="mobile">
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Ciudad</label>
            <input type="text" class="form-control" id="city" name="city">
        </div>

        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Dirección</label>
            <input type="text" class="form-control" id="direction" name="direction">
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Correo Principal</label>
            <input type="text" class="form-control" id="main_email" name="main_email">
        </div>

        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Correo Notificaciones</label>
            <input type="text" class="form-control" id="email_notificaions" name="email_notificaions">
        </div>

        <div class="col-md-4">
            <label for="inputCity" class="form-label" style="color:#008CFE;">Sítio Web</label>
            <input type="text" class="form-control" id="web_site" name="web_site">
        </div>
        <div class="col-md-12">
            <label for="exampleFormControlTextarea1" class="form-label" style="color:#008CFE;">Comentarios</label>
            <textarea class="form-control" id="comments" name="comments" rows="3" ></textarea>
        </div>

        <div class="col-12">
            <a href="/register/index" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary" tabindex="4">Guardar Empresa</button>
        </div>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

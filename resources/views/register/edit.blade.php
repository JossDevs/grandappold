@extends('adminlte::page')

@section('title', 'Empresas')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Actualizar Empresas</b></h3>
@stop

@section('content')


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>


    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Editar de Empresa</b></h5>

        <form action="{{route('register.update',$register->id)}}" method="POSt" role="form" class="form-horizontal">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo_Empresa</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="person_type" name="person_type" >
                                    <option>_Seleccione Empresa</option>
                                    <option value="Empresa Pública" {{($register->person_type ==='Empresa Pública') ? 'selected' : ''}}> Empresa Publica </option>
                                    <option value="Empresa Privada" {{($register->person_type ==='Empresa Privada') ? 'selected' : ''}}> Empresa Privada </option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Razón_Social</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="business_name" name="business_name" placeholder="Nombre" value="{{$register->business_name}}" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Comercial</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="tradename" name="tradename" placeholder="Nombre" value="{{$register->tradename}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Cédula/Ruc</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="id_ruc" name="id_ruc" placeholder="Cédula" value="{{$register->id_ruc}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Teléfono</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="telephone" name="telephone" placeholder="Teléfono" value="{{$register->telephone}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Celular</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="mobile"  name="mobile" placeholder="Celular" value="{{$register->mobile}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Provincia</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="province" name="province" value="{{$register->province}}">
                                    <option>_Seleccione Provincia</option>
                                    <option value="Azuay">Azuay</option>
                                    <option value="Bolivar">Bolivar</option>
                                    <option value="Cañar">Cañar</option>
                                    <option value="Carchi">Carchi</option>
                                    <option value="Cotopaxi">Cotopaxi</option>
                                    <option value="Chimborazo">Chimborazo</option>
                                    <option value="El Oro">El Oro</option>
                                    <option value="Esmeraldas">Esmeraldas</option>
                                    <option value="Guayas">Guayas</option>
                                    <option value="Imbabura">Imbabura</option>
                                    <option value="Loja">Loja</option>
                                    <option value="Los Ríos">Los Ríos</option>
                                    <option value="Manabí">Manabí</option>
                                    <option value="Morona Santiago">Morona Santiago</option>
                                    <option value="Napo">Napo</option>
                                    <option value="Pastaza">Pastaza</option>
                                    <option value="Pichincha">Pichincha</option>
                                    <option value="Tungurahua">Tungurahua</option>
                                    <option value="Zamora Chinchipe">Zamora Chinchipe</option>
                                    <option value="Galápagos">Galápagos</option>
                                    <option value="Sucumbios">Sucumbios</option>
                                    <option value="Orellana">Orellana</option>
                                    <option value="Santo Domingo">Santo Domingo</option>
                                    <option value="Santa Elena">Santa Elena</option>
                                    <option value="Zonas No Demilitadas">Zonas No Demilitadas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Ciudad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="city" name="city" placeholder="Ciudad" value="{{$register->city}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Dirección</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="direction" name="direction" placeholder="Dirección" value="{{$register->direction}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Correo_Principal</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control-sm" id="main_email" name="main_email" placeholder="Correo_Principal" value="{{$register->main_email}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Correo_Notificaciones</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="email_notificaions" name="email_notificaions" placeholder="Correo_Notificaciones" value="{{$register->email_notificaions}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Sítio_Web</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="web_site" name="web_site" placeholder="Sítio_Web" value="{{$register->web_site}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Comentario</label>
                    <div class="col-md-12">
                        <textarea type="text" class="form-control" id="comments" name="comments" rows="2"  cols="59" value="{{$register->comments}}"></textarea>
                    </div>
                </div>
                <div class="col-md-12" >
                    <h5 style="color:#5D676F;" ><b>Registro Representante Legal</b></h5>
                </div>


                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="legal_representative" name="legal_representative" placeholder="Nombre" value="{{$register->legal_representative}}"/>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Cédula_Rep_legal</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="id_legal_representative" name="id_legal_representative" placeholder="ID" value="{{$register->id_legal_representative}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Contador</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="accountant" name="accountant" placeholder="Contador" value="{{$register->accountant}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Ruc_contador</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="ruc_counter" name="ruc_counter" placeholder="Ruc" value="{{$register->ruc_counter}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N_Exp_Supercias</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="exp_Surpluses"  name="exp_Surpluses" placeholder="Supercias" value="{{$register->exp_Surpluses}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Redes_Sociales</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="social_media" name="social_media" value="{{$register->social_media}}">
                                    <option>_Seleccione Redes_S</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Twitter">Twitter</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Discapacidad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="disability" name="disability" value="{{$register->disability}}">
                                    <option>_Seleccione_Discapa</option>
                                    <option value="Empresa 1">1%</option>
                                    <option value="Empresa 2">2%</option>
                                    <option value="Empresa 3">3%</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Porcentaje_(%)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="percentage" name="percentage" placeholder="Porcentaje" value="{{$register->percentage}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Constitución</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="f_constitution" name="f_constitution"
                               value="{{$register->f_constitution}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N.</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="n" name="n" placeholder="N" value="{{$register->n}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Resol_N</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="resol_n" name="resol_n" placeholder="Resol_N" value="{{$register->resol_n}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Código_N</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="code_n" name="code_n" placeholder="Código_N" value="{{$register->code_n}}"/>
                    </div>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="Si" id="keep_accounting" name="keep_accounting" value="{{$register->keep_accounting}}" >
                    <label class="form-check-label" for="flexCheckDefault">
                        Obligado a llevar Contabilidad
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="Si" id="general_regime" name="general_regime" value="{{$register->general_regime}}" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Régimen General
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="SI" id="microenterprise" name="microenterprise" value="{{$register->microenterprise}}">
                    <label class="form-check-label" for="flexCheckDefault">
                        Microempresa
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="Si" id="special_taxpayer" name="special_taxpayer" value="{{$register->special_taxpayer}}" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Contibuyente Especial
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="SI" id="withholding_agent" name="withholding_agent" value="{{$register->withholding_agent}}">
                    <label class="form-check-label" for="flexCheckDefault">
                        Agente de Retención
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="Si" id="is_exporter" name="is_exporter" value="{{$register->is_exporter}}" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Es Exportador
                    </label>
                </div>
                <div class="col-sm-12">
                    <button type="submit"  class="btn btn-primary btn-sm">
                        Guardar Registro</button>
                    <button  href="{{route('register.index')}}" type="button" class="btn btn-default btn-sm">
                        Cancelar</button>

                </div>



            </div>


        </form>


    </div>












    </body>


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

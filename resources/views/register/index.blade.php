@extends('adminlte::page')

@section('title', 'Empresas')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Administración de las Empresas</b></h3>
@stop

@section('content')



    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>
        @livewireStyles

    </head>
    <body>

    <!-- Button trigger modal -->


    <!-- Large modal -->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 style="color:#5D676F;"><b>Ingreso Datos</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" >
                            <!-- Nav tabs -->
                            <nav class="nav nav-pills nav-justified">
                                <a class="nav-link active" href="#Login" data-toggle="tab">Empresa</a>
                            </nav>
                            <!-- Tab panes -->
                            <div class="tab-content ">
                                <div class="tab-pane active " id="Login" >
                                    <form action="register" method="POST" role="form" class="form-horizontal">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <label for="email" class="col-sm-2 control-label">
                                                    Tipo-Empresa</label>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <select class="form-control" id="person_type" name="person_type">
                                                                <option>Seleccione</option>
                                                                <option value="Empresa 1">Empresa 1</option>
                                                                <option value="Empresa 2">Empresa 2</option>
                                                                <option value="Empresa 3">Empresa 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Rázon_Social</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="business_name" name="business_name" placeholder="Razón_Social" />
                                                </div>
                                            </div>
                                        <div class="form-group ">
                                            <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                Nombre_Comercial</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="tradename" name="tradename" placeholder="Nombre_Comercial" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                Cédula/Ruc</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" id="id_ruc" name="id_ruc" placeholder="Cédula/Ruc" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                Teléfono</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" id="telephone" name="telephone" placeholder="Teléfono" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                Celular</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" id="mobile"  name="mobile" placeholder="Celular" />
                                            </div>
                                        </div>
                                            <div class="form-group ">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Provincia</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="province" name="province" placeholder="Provincia" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Ciudad</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="city" name="city" placeholder="Ciudad" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Dirección</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="direction" name="direction" placeholder="Dirección" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Correo_Principal</label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" id="main_email" name="main_email" placeholder="Correo_Principal" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Correo_Notificaciones</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="email_notificaions" name="email_notificaions" placeholder="Correo_Notificaciones" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Sítio_Web</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="web_site" name="web_site" placeholder="Sítio_Web" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                    Comentario</label>
                                                <div class="col-md-10">
                                                    <textarea type="text" class="form-control" id="comments" name="comments" rows="2" ></textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    Guardar & Continuar</button>
                                                <a href="javascript:;"></a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="Registration">
                                    <form role="form" class="form-horizontal">
                                        <div class="row">
                                        <div class="col-sm-10">
                                            <label for="email" class="col-sm-2 control-label">
                                                Tipo_Sucursal</label>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <select class="form-control" id="person_type" name="person_type">
                                                            <option>Seleccione</option>
                                                            <option value="Sucursal 1">Sucursal 1</option>
                                                            <option value="Sucursal 2">Sucursal 2</option>
                                                            <option value="Sucursal 3">Sucursal 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-2 control-label">
                                                N-Sucursal</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="email" placeholder="Sucursal" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="mobile" class="col-sm-2 control-label">
                                                Nombre_Comercial</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="mobile" placeholder="Nombre_Comercial" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Pais</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Pais" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Provincia</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Password" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Ciudad</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Ciudad" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Teléfono</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Teléfono" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Celular</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Celular" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">
                                                Dirección</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" placeholder="Dirección" />
                                            </div>
                                        </div>

                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-10">
                                                <button type="button" class="btn btn-primary btn-sm">
                                                    Guardar & Continuar</button>
                                                <button type="button" class="btn btn-default btn-sm">
                                                    Cancelar</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Registros de Empresas</b></h5>

        <form action="register" method="POST" role="form" class="form-horizontal">
            @csrf
            <div class="row">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">

                        Tipo_Empresa</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="person_type" name="person_type">
                                    <option>_Seleccione Empresa</option>
                                    <option value="publica" >Empresa Pública</option>
                                    <option value="privada">Empresa Privada</option>
                                    <option value="persona" >Persona Natural</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Razón_Social</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="business_name" name="business_name" placeholder="Razón_Social" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Comercial</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="tradename" name="tradename" placeholder="Nombre_Comercial" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Cédula/Ruc</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="id_ruc" name="id_ruc" placeholder="Cédula/Ruc" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Teléfono</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="telephone" name="telephone" placeholder="Teléfono" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Celular</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="mobile"  name="mobile" placeholder="Celular" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Provincia</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select   class="form-control-sm" id="province" name="province">
                                    <option>_Seleccione_Provincia</option>
                                    @foreach($provinces as $province)
                                        <option value="{{$province->id_provinces}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                @if(!is_null($cities))
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Ciudad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select  class="form-control-sm" id="city" name="city">
                                    <option>__Seleccione_Ciudad</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id_cities}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @endif



                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Dirección</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="direction" name="direction" placeholder="Dirección" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Correo_Principal</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control-sm" id="main_email" name="main_email" placeholder="Correo_Principal" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Correo_Notificaciones</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="email_notificaions" name="email_notificaions" placeholder="Correo_Notificaciones" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Sítio_Web</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="web_site" name="web_site" placeholder="Sítio_Web" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Comentario</label>
                    <div class="col-md-12">
                        <textarea type="text" class="form-control" id="comments" name="comments" rows="2"  cols="59" ></textarea>
                    </div>
                </div>
                <div class="col-md-12" >
                    <h5 style="color:#5D676F;" ><b>Registro Representante Legal</b></h5>
                </div>


                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="legal_representative" name="legal_representative" placeholder="Nombre" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Cédula_Rep_legal</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="id_legal_representative" name="id_legal_representative" placeholder="ID" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Contador</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="accountant" name="accountant" placeholder="Nombre" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Ruc_contador</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="ruc_counter" name="ruc_counter" placeholder="Ruc" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N_Exp_Supercias</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="exp_Surpluses"  name="exp_Surpluses" placeholder="Supercias" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Redes_Sociales</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="social_media" name="social_media">
                                    <option>_Seleccione Redes_S</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Twitter">YouTube</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="WhatsApp">Facebook Messenger</option>
                                    <option value="WhatsApp">Wechat</option>
                                    <option value="WhatsApp">Instagram</option>
                                    <option value="WhatsApp">QQ</option>
                                    <option value="WhatsApp">QZone</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Discapacidad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="disability" name="disability">
                                    <option>_Seleccione_Discapa</option>
                                    <option value="Inválido">Inválido</option>
                                    <option value="Derrame ">Derrame</option>
                                    <option value="Colesterol ">Colesterol</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Porcentaje_(%)</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="percentage" name="percentage" placeholder="Porcentaje" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Constitución</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="f_constitution" name="f_constitution"
                               required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N.</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="n" name="n" placeholder="N" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Resol_N</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="resol_n" name="resol_n" placeholder="Resol_N" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Código_N</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="code_n" name="code_n" placeholder="Código_N" />
                    </div>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="keep_accounting" name="keep_accounting">
                    <label class="form-check-label" for="flexCheckDefault">
                        Obligado a llevar Contabilidad
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="general_regime" name="general_regime" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Régimen General
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="microenterprise" name="microenterprise">
                    <label class="form-check-label" for="flexCheckDefault">
                        Microempresa
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="special_taxpayer" name="special_taxpayer" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Contibuyente Especial
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="withholding_agent" name="withholding_agent">
                    <label class="form-check-label" for="flexCheckDefault">
                        Agente de Retención
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="is_exporter" name="is_exporter" >
                    <label class="form-check-label" for="flexCheckChecked">
                        Es Exportador
                    </label>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Guardar Registro</button>

                </div>


            </div>


        </form>


        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">


                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Empresas</b></h2>
                        </div>


                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Tipo Empresa</th>
                        <th>Razón Social</th>
                        <th>Nombre</th>
                        <th>ID/RUC</th>
                        <th>Teléfono</th>
                        <th>Celular</th>
                        <th>Provincia</th>
                        <th>Ciudad</th>
                        <th>Dirección</th>
                        <th>Email</th>
                        <th>Email_Secund</th>
                        <th>Sitio Web</th>
                        <th>Comentarios</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                           @foreach($registers as $register)
                               <tr>
                                   <td>{{$register->person_type}}</td>
                                   <td>{{$register->business_name}}</td>
                                   <td>{{$register->tradename}}</td>
                                   <td>{{$register->id_ruc}}</td>
                                   <td>{{$register->telephone}}</td>
                                   <td>{{$register->mobile}}</td>
                                   <td>{{$register->province}}</td>
                                   <td>{{$register->city}}</td>
                                   <td>{{$register->direction}}</td>
                                   <td>{{$register->main_email}}</td>
                                   <td>{{$register->email_notificaions}}</td>
                                   <td>{{$register->web_site}}</td>
                                   <td>{{$register->comments}}</td>
                                   <td>
                                       <form action="{{route('register.destroy',$register->id)}}" method="POST" >
                                           @csrf
                                           @method('DELETE')
                                       <a href="register/{{$register->id}}/edit" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                           <button type="submit" class="delete" title="Eliminar" ><i class="fas fa-trash-alt" ></i></button>
                                       <a href="{{route('branch.index',array('registerId' => $register->id))}}" class="delete" title="Ver Sucursales" data-toggle="tooltip"><i class="fab fa-accusoft">&#xE5C9;</i></a>
                                       </form>
                                   </td>

                               </tr>
                           @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>







    <div class="container-xl">

        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">

                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Representante Legal</b></h2>
                        </div>
                        <div class="col-sm-7">
                            <a data-toggle="modal" data-target="#myModal" class="btn btn-secondary"><i class="fas fa-id-badge">&#xE147;</i> <span>Ingresar Cliente</span></a>

                        </div>

                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Representante</th>
                        <th>Cédula</th>
                        <th>O. Contabilidad</th>
                        <th>Régimen G</th>
                        <th>Microempresa</th>
                        <th>C. Especial</th>
                        <th>Agente Retn</th>
                        <th>Es Exportador</th>
                        <th>Contador</th>
                        <th>Ruc contador</th>
                        <th>Exp_Supercia</th>
                        <th>Redes Sociales</th>
                        <th>Discapacidad</th>
                        <th>Porct</th>
                        <th>Fecha Const</th>
                        <th>N</th>
                        <th>Resol_N</th>
                        <th>Código_N</th>

                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($registers as $register)
                        <tr>
                            <td>{{$register->legal_representative}}</td>
                            <td>{{$register->id_legal_representative}}</td>
                            <td>{{$register->keep_accounting}}</td>
                            <td>{{$register->general_regime}}</td>
                            <td>{{$register->microenterprise}}</td>
                            <td>{{$register->special_taxpayer}}</td>
                            <td>{{$register->withholding_agent}}</td>
                            <td>{{$register->is_exporter}}</td>
                            <td>{{$register->accountant}}</td>
                            <td>{{$register->ruc_counter}}</td>
                            <td>{{$register->exp_Surpluses}}</td>
                            <td>{{$register->social_media}}</td>
                            <td>{{$register->disability}}</td>
                            <td>{{$register->percentage}}</td>
                            <td>{{$register->f_constitution}}</td>
                            <td>{{$register->n}}</td>
                            <td>{{$register->resol_n}}</td>
                            <td>{{$register->code_n}}</td>
                            <td>

                                <a href="#" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                <a href="#" class="delete" title="Eliminar" data-toggle="tooltip"><i class="fas fa-trash-alt">&#xE5C9;</i></a>

                            </td>


                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    @livewireScripts

    </body>

    <script>
        public function nuevaEmpresa(){
            $('#myModal').modal('show');
            // www.jquery2dotnet.com

        }

    </script>

    @stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>

@stop

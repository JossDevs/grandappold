@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Editar  Productos</b></h3>
@stop

@section('content')


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>




    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Actualice  Productos</b></h5>

        <form action="product/{{$product->id}}" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo_Producto</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="product_type" name="product_type"  value="{{$product->main_code}}">
                                    <option>_Seleccione Producto</option>
                                    <option value="Bien">Bien</option>
                                    <option value="Servicio">Servicio</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Categoria</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="category" name="category"  value="{{$product->main_code}}">
                                    <option>Seleccione Categoria</option>
                                    <option value="Categoria 1">Categoria 1</option>
                                    <option value="Categoria 2">Categoria 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Código_Principal</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="main_code" name="main_code" placeholder="Principal"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Código_Auxiliar</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="auxiliary_code" name="auxiliary_code" placeholder="Auxiliar"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Nombre_Producto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="product_name" name="product_name" placeholder="Nombre"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Marca</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="brand" name="brand"  value="{{$product->main_code}}">
                                    <option>__Seleccione /Marca</option>
                                    <option value="Dell">Dell</option>
                                    <option value="Sony">Sony</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Modelo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="model"  name="model" placeholder="Modelo"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Unidad_Medida</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="unit_measure" name="unit_measure"  value="{{$product->main_code}}">
                                    <option>_Seleccione / Unidad</option>
                                    <option value="Megabytes">Megabytes</option>
                                    <option value="Gigabytes">Gigabytes</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Subtotal_PVP</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control-sm" id="subtotal" name="subtotal" placeholder="Subtotal"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        IVA</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="iva" name="iva" placeholder="IVA"
                               value="{{$product->main_code}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        ICE</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control-sm" id="ice" name="ice" placeholder="ICE" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Estado</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="status" name="status" placeholder="Estado" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Inicio</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="start_date" name="start_date"
                               required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Fin</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="finish_date" name="finish_date"
                               required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Promedio</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="average" name="average" placeholder="Promedio" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Comentario</label>
                    <div class="col-md-12">
                        <textarea type="text" class="form-control" id="comments" name="comments" rows="2"  cols="59" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="logo" class="col-sm-2 control-label">
                        Fotos</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control-sm" id="photo" name="photo" accept="image/*" />
                        @error('file')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Guardar Registro</button>

                </div>


            </div>


        </form>


        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">


                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Listado_Productos</b></h2>
                        </div>


                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Tipo Producto</th>
                        <th>Categoria</th>
                        <th>Codigo Principal</th>
                        <th>Código Auxiliar</th>
                        <th>Nombre Producto</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Unidad Medida</th>
                        <th>Subtotal</th>
                        <th>IVA</th>
                        <th>ICE</th>
                        <th>Estado</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Final</th>
                        <th>Promedio</th>
                        <th>Foto</th>
                        <th>Comentario</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->product_type}}</td>
                            <td>{{$product->category}}</td>
                            <td>{{$product->main_code}}</td>
                            <td>{{$product->auxiliary_code}}</td>
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->brand}}</td>
                            <td>{{$product->model}}</td>
                            <td>{{$product->unit_measure}}</td>
                            <td>{{$product->subtotal}}</td>
                            <td>{{$product->iva}}</td>
                            <td>{{$product->ice}}</td>
                            <td>{{$product->status}}</td>
                            <td>{{$product->start_date}}</td>
                            <td>{{$product->finish_date}}</td>
                            <td>{{$product->average}}</td>

                            <td>
                                <img src="{{asset($product->photo)}}" alt="{{$product->title}}" class="img-fluid" width="40px" height=="40px" style="border-radius:20px" >
                            </td>

                            <td>{{$product->comments}}</td>
                            <td>
                                <a href="#" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                <a href="#" class="delete" title="Eliminar" data-toggle="tooltip"><i class="fas fa-trash-alt">&#xE5C9;</i></a>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    </body>

    <script>
        public function nuevaEmpresa(){
            $('#myModal').modal('show');
            // www.jquery2dotnet.com

        }

    </script>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
<?php

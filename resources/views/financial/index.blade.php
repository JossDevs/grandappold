@extends('adminlte::page')

@section('title', 'Financiero')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Registro Financiero</b></h3>
@stop

@section('content')


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>


    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Datos Financieros</b></h5>

        <form action="financial" method="POST" role="form" class="form-horizontal guardarFormulario">
            @csrf
            <div class="row">

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Elija_Empresa</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="registers_id" name="registers_id">
                                    <option>Seleciones</option>
                                    @foreach($registers as $register)
                                        <option value="{{$register->id}}">{{$register->business_name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Categoría_Cuenta</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="account_category" name="account_category">
                                    <option>_Seleccione Categoría</option>
                                    <option value="Local">Local</option>
                                    <option value="Exterior">Exterior</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Entidad_Financiera</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select   class="form-control-sm" id="financial_entity" name="financial_entity">
                                    <option>_Seleccione_Entidad_F</option>
                                    @foreach($financialEntities as $financialEntity)
                                        <option value="{{$financialEntity->financial_entities}}">{{$financialEntity->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo_Cuenta</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="account_type" name="account_type">
                                    <option>_Seleccione Cuenta</option>
                                    <option value="Corriente">Corriente</option>
                                    <option value="Ahorros">Ahorros</option>
                                    <option value="Tarjeta Crédito">Tarjeta Crédito</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        N_Cuenta</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="account_card_number" name="account_card_number" placeholder="N_Cuenta" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Nombre_Tarjeta</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="card_name" name="card_name">
                                    <option>_Seleccione Tarjeta</option>
                                    <option value="Visa">Visa</option>
                                    <option value="Mastercard">Mastercard</option>
                                    <option value="Diners">Diners</option>
                                    <option value="American Express">American Express</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Cupo_Tarjeta</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="card_quota" name="card_quota" placeholder="Cupo" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Plan_cuentas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="accounting_account" name="accounting_account" placeholder="Plan" />
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-sm ">
                        Guardar Registro</button>

                </div>


            </div>


        </form>


        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">


                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Empresas</b></h2>
                        </div>


                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Empresa</th>
                        <th>Categoría_Cuenta</th>
                        <th>Entidad_Financiera</th>
                        <th>Tipo_Cuenta</th>
                        <th>N-Cuenta/Tarjeta</th>
                        <th>Nombre_Tarjeta</th>
                        <th>Cupo_Tarjeta</th>
                        <th>Cuenta_Contable</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($financials as $financial)
                        <tr>
                            @foreach($registers as $register)
                                <td>{{$register->business_name}}</td>
                            @endforeach
                            <td>{{$financial->account_category}}</td>
                            <td>{{$financial->financial_entity}}</td>
                            <td>{{$financial->account_type}}</td>
                            <td>{{$financial->account_card_number}}</td>
                            <td>{{$financial->card_name}}</td>
                            <td>{{$financial->card_quota}}</td>
                            <td>{{$financial->accounting_account}}</td>

                            <td>
                                <a href="register/{{$register->id}}/edit" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                <a href="#" class="delete" title="Eliminar" data-toggle="tooltip"><i class="fas fa-trash-alt">&#xE5C9;</i></a>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    </body>

    <script>
        public function nuevaEmpresa(){
            $('#myModal').modal('show');
            // www.jquery2dotnet.com

        }

    </script>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>

        $('.guardarFormulario').submit(function (){

            //e.preventDefault();
            Swal.fire(
                'Registro Insertado',
                'Se Guardo Correctamente!',
                'success'
            )

            //this.submit();

        })





    </script>
@stop


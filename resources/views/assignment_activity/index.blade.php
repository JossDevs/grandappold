@extends('adminlte::page')

@section('title', 'Actividades')

@section('content_header')
    <h3 style="color:#2196F3;"><b>Registro de Actividades</b></h3>
@stop

@section('content')


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>




        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 12px;
            }
            .container-xl{
                font-size: 8px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #D4DEE7;
                color: #060606;
                padding: 8px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 12px;
            }
            .table-title .btn {
                color: #5D676F;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #D4DEE7;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 5px 5px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 10px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>


    </head>
    <body>

    <div class="container-xl">
        <h5 style="color:#5D676F;"><b>Ingrese Actividades</b></h5>

        <form action="assignment_activity" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Tipo_Actividad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="activity_type" name="activity_type">
                                    <option>Seleccione/Actividad</option>
                                    <option value="Eventual">Eventual</option>
                                    <option value="Recurrente">Recurrente</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Periocidad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="periodicity" name="periodicity">
                                    <option>_Seleccione /Periodo</option>
                                    <option value="Diaria">Diaria</option>
                                    <option value="Semanal">Semanal</option>
                                    <option value="Quincena">Quincena</option>
                                    <option value="Mensual">Mensual</option>
                                    <option value="Trimestral">Trimestral</option>
                                    <option value="Semestral">Semestral</option>
                                    <option value="Anual">Anual</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Servicio</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="service" name="service">
                                    <option>_Seleccione / Servicio</option>
                                    <option value="Servicio 1">Servicio 1</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Responsable</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="responsable" name="responsable">
                                    <option>_Seleccione Resp/Eq</option>
                                    <option value="Equipo 1">Equipo 1</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Actividad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="activity" name="activity">
                                    <option>Seleccione/ Actividad</option>
                                    <option value="Actividad 1">Actividad 1</option>
                                    <option value="Actividad 2">Actividad 2</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Inicio</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="start_date" name="start_date"
                               required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Fecha_Fin</label>
                    <div class="col-sm-12">
                        <input class="form-control-sm" type="date" id="finish_date" name="finish_date"
                               required value="{{old('scheduled_date',$now->format('Y-m-d'))}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Prioridad</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="priority" name="priority">
                                    <option>_Seleccione Prioridad</option>
                                    <option value="Baja">Baja</option>
                                    <option value="Media">Media</option>
                                    <option value="Alta">Alta</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Orden</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="order" name="order" placeholder="Orden" />
                    </div>
                </div>
                <div class="form-group ">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Tiempo_Estimado</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="estimated_time" name="estimated_time" placeholder="Tiempo" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">
                        Estado</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-10">
                                <select class="form-control-sm" id="condition" name="condition">
                                    <option>__Seleccione Estado</option>
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Avance</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control-sm" id="advance" name="advance" placeholder="Avance" />
                    </div>
                </div>


                <div class="form-group">
                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                        Comentario</label>
                    <div class="col-md-14">
                        <textarea type="text" class="form-control" id="comments" name="comments" rows="2"  cols="59" ></textarea>
                    </div>
                </div>


                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Guardar Registro</button>

                </div>


            </div>


        </form>


        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">


                    <div class="row">
                        <div class="col-sm-5">
                            <h2> <b>Listado_Productos</b></h2>
                        </div>


                    </div>
                </div>
                <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                        <th>Tipo_Actividad</th>
                        <th>Periocidad</th>
                        <th>Servicio</th>
                        <th>Responsable</th>
                        <th>Actividad</th>
                        <th>Fecha_Inicio</th>
                        <th>Fecha_Final</th>
                        <th>Prioridad</th>
                        <th>Orden</th>
                        <th>Tiempo_Estimado</th>
                        <th>Estado</th>
                        <th>Avance</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($assignment_activities as $assignment_activity)
                        <tr>
                            <td>{{$assignment_activity->activity_type}}</td>
                            <td>{{$assignment_activity->periodicity}}</td>
                            <td>{{$assignment_activity->service}}</td>
                            <td>{{$assignment_activity->responsable}}</td>
                            <td>{{$assignment_activity->activity}}</td>
                            <td>{{$assignment_activity->start_date}}</td>
                            <td>{{$assignment_activity->finish_date}}</td>
                            <td>{{$assignment_activity->priority}}</td>
                            <td>{{$assignment_activity->order}}</td>
                            <td>{{$assignment_activity->estimated_time}}</td>
                            <td>{{$assignment_activity->condition}}</td>
                            <td>{{$assignment_activity->advance}}</td>
                            <td>{{$assignment_activity->comments}}</td>

                            <td>
                                <a href="#" class="settings" title="Editar" data-toggle="tooltip"><i class="fas fa-user-edit">&#xE8B8;</i></a>
                                <a href="#" class="delete" title="Eliminar" data-toggle="tooltip"><i class="fas fa-trash-alt">&#xE5C9;</i></a>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="hint-text">Página <b>1</b> de <b>2</b> Entradas</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Anterior</a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    </body>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop


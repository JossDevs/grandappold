<?php

namespace App\Http\Livewire;

use App\Models\Province;
use Livewire\Component;
use App\Models\City;

class SelectComponent extends Component
{

    public  $selectedProvince = null, $selectedCity = null;

    public $cities=null;
    public function render()
    {
        return view('register.index',[
        'provinces'=>Province::all()
            ]);

    }

    public function updatedselectedProvince($province_id){
        $this->cities= City::where('province_id',$province_id)->get();

    }


}

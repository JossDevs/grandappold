<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Client;
use App\Models\Country;
use App\Models\Province;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $clients=Client::all();
        $provinces=Province::all();
        $cities=City::all();
        $now=Carbon::now();
        return view('client.index')->with(compact('registers',$registers,
            'now','provinces',$provinces,'cities',$cities,'clients',$clients));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $clients = new Client();
        $clients->person_type=$request->get('person_type');
        $clients->business_name=$request->get('business_name');
        $clients->tradename=$request->get('tradename');
        $clients->id_ruc=$request->get('id_ruc');
        $clients->telephone=$request->get('telephone');
        $clients->mobile=$request->get('mobile');
        $clients->province=$request->get('province');
        $clients->city=$request->get('city');
        $clients->direction=$request->get('direction');
        $clients->main_email=$request->get('main_email');
        $clients->email_notificaions=$request->get('email_notificaions');
        $clients->web_site=$request->get('web_site');
        $clients->comments=$request->get('comments');

        $clients->legal_representative=$request->get('legal_representative');
        $clients->id_legal_representative=$request->get('id_legal_representative');
        $clients->keep_accounting=$request->get('keep_accounting');
        $clients->general_regime=$request->get('general_regime');
        $clients->microenterprise=$request->get('microenterprise');
        $clients->special_taxpayer=$request->get('special_taxpayer');
        $clients->withholding_agent=$request->get('withholding_agent');
        $clients->is_exporter=$request->get('is_exporter');
        $clients->accountant=$request->get('accountant');
        $clients->ruc_counter=$request->get('ruc_counter');
        $clients->exp_Surpluses=$request->get('exp_Surpluses');
        $clients->social_media=$request->get('social_media');
        $clients->disability=$request->get('disability');
        $clients->percentage=$request->get('percentage');
        $clients->f_constitution=$request->get('f_constitution');
        $clients->n=$request->get('n');
        $clients->resol_n=$request->get('resol_n');
        $clients->code_n=$request->get('code_n');



        $clients->save();

        return  redirect('/client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

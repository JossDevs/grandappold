<?php

namespace App\Http\Controllers;

use App\Models\AssignmentService;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AssignmentServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $assignment_services=AssignmentService::all();
        $now=Carbon::now();
        return view('assignmentservice.index')->with(compact('registers',$registers,
            'now','assignment_services',$assignment_services));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $assignment_services= new AssignmentService();

        $assignment_services->service=$request->get('service');
        $assignment_services->adviser=$request->get('adviser');
        $assignment_services->rate_type=$request->get('rate_type');
        $assignment_services->rate_period=$request->get('rate_period');
        $assignment_services->comments=$request->get('comments');
        $assignment_services->subtotal=$request->get('subtotal');
        $assignment_services->iva=$request->get('iva');
        $assignment_services->total=$request->get('total');
        $assignment_services->status=$request->get('status');
        $assignment_services->start_date=$request->get('start_date');
        $assignment_services->finish_date=$request->get('finish_date');


        $assignment_services->save();

        return  redirect('/assignment_service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

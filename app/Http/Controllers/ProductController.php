<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $products=Product::all();
        $now=Carbon::now();
        return view('product.index')->with(compact('registers',$registers,'now','products',$products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products= new Product();



        $products->product_type=$request->get('product_type');
        $products->category=$request->get('category');
        $products->main_code=$request->get('main_code');
        $products->auxiliary_code=$request->get('auxiliary_code');
        $products->product_name=$request->get('product_name');
        $products->brand=$request->get('brand');
        $products->model=$request->get('model');
        $products->unit_measure=$request->get('unit_measure');
        $products->subtotal=$request->get('subtotal');
        $products->iva=$request->get('iva');
        $products->ice=$request->get('ice');
        $products->comments=$request->get('comments');
        $products->status=$request->get('status');
        $products->start_date=$request->get('start_date');
        $products->finish_date=$request->get('finish_date');
        $products->average=$request->get('average');

        if($request->hasFile('photo')){
            $file=$request->file('photo');
            $destinationPath='images/featureds/';
            $filename=time() . '_' . $file->getClientOriginalName();
            $uploadSuccess=$request->file('photo')->move($destinationPath,$filename);
            $products->photo=$destinationPath . $filename;
        }



        $products->save();

        return  redirect('/product');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

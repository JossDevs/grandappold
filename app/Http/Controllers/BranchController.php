<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\City;
use App\Models\Country;
use App\Models\Province;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        $registerid = $request->registerId;
        $branches=Branch::where('registers_id','=',$registerid )->get();
        $countries=Country::all();
        $provinces=Province::all();
        $cities=City::all();
        $now=Carbon::now();
        return view('branch_offices.index')->with(compact('branches',
            'provinces','countries','branches','cities','now', 'registerid'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $branches = new Branch();



        $branches->type=$request->get('type');
        $branches->branch=$request->get('branch');
        $branches->name=$request->get('name');

        if($request->hasFile('logo')){
            $file=$request->file('logo');
            $destinationPath='images/featureds/';
            $filename=time() . '_' . $file->getClientOriginalName();
            $uploadSuccess=$request->file('logo')->move($destinationPath,$filename);
            $branches->logo=$destinationPath . $filename;
        }

        if($request->hasFile('firm')){
            $file2=$request->file('firm');
            $destinationPath2='images/featureds/';
            $filename2=time() . '-' . $file2->getClientOriginalName();
            $uploadSuccess=$request->file('firm')->move($destinationPath2,$filename2);
            $branches->firm=$destinationPath2 . $filename2;
        }


        $branches->country=$request->get('country');
        $branches->province=$request->get('province');
        $branches->city=$request->get('city');
        $branches->telephone=$request->get('telephone');
        $branches->phone=$request->get('phone');
        $branches->direction=$request->get('direction');
        $branches->email=$request->get('email');
        $branches->website=$request->get('website');
        $branches->social_media=$request->get('social_media');
        $branches->activity=$request->get('activity');
        $branches->start_date=$request->get('start_date');
        $branches->final_date=$request->get('final_date');
        $branches->registers_id=$request->get('registers_id');

        $branches->save();



        return  redirect('/branch');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_branches)
    {
        $branch =Branch::find($id_branches);
        $countries=Country::all();
        $provinces=Province::all();

        $cities=City::all();
        return view('branch_offices.edit')->with(compact('branch','countries','provinces','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_branches)
    {
        $branch = Branch::find($id_branches);



        $branch->type=$request->get('type');
        $branch->branch=$request->get('branch');
        $branch->name=$request->get('name');

        if($request->hasFile('logo')){
            $file=$request->file('logo');
            $destinationPath='images/featureds/';
            $filename=time() . '_' . $file->getClientOriginalName();
            $uploadSuccess=$request->file('logo')->move($destinationPath,$filename);
            $branch->logo=$destinationPath . $filename;
        }

        if($request->hasFile('firm')){
            $file2=$request->file('firm');
            $destinationPath2='images/featureds/';
            $filename2=time() . '-' . $file2->getClientOriginalName();
            $uploadSuccess=$request->file('firm')->move($destinationPath2,$filename2);
            $branch->firm=$destinationPath2 . $filename2;
        }


        $branch->country=$request->get('country');
        $branch->province=$request->get('province');
        $branch->city=$request->get('city');
        $branch->telephone=$request->get('telephone');
        $branch->phone=$request->get('phone');
        $branch->direction=$request->get('direction');
        $branch->email=$request->get('email');
        $branch->website=$request->get('website');
        $branch->social_media=$request->get('social_media');
        $branch->activity=$request->get('activity');
        $branch->start_date=$request->get('start_date');
        $branch->final_date=$request->get('final_date');
        $branch->registers_id=$request->get('registers_id');

        $branch->save();



        return  redirect('/branch');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::where('id_branches',$id)->first();
        $branch->delete();

        return redirect('/branch')->with('success', 'Stock has been deleted Successfully');
    }
}

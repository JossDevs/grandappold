<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Country;
use App\Models\Province;
use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Register;
use Livewire\Component;



class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $registers=Register::all();
        $provinces=Province::all();
        $cities=City::all();
        $now=Carbon::now();
        return view('register.index')->with(compact('registers',$registers,'now','provinces',$provinces,'cities',$cities));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('register.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registers = new Register();
        $registers->person_type=$request->get('person_type');
        $registers->business_name=$request->get('business_name');
        $registers->tradename=$request->get('tradename');
        $registers->id_ruc=$request->get('id_ruc');
        $registers->telephone=$request->get('telephone');
        $registers->mobile=$request->get('mobile');
        $registers->province=$request->get('province');
        $registers->city=$request->get('city');
        $registers->direction=$request->get('direction');
        $registers->main_email=$request->get('main_email');
        $registers->email_notificaions=$request->get('email_notificaions');
        $registers->web_site=$request->get('web_site');
        $registers->comments=$request->get('comments');

        $registers->legal_representative=$request->get('legal_representative');
        $registers->id_legal_representative=$request->get('id_legal_representative');
        $registers->keep_accounting=$request->get('keep_accounting');
        $registers->general_regime=$request->get('general_regime');
        $registers->microenterprise=$request->get('microenterprise');
        $registers->special_taxpayer=$request->get('special_taxpayer');
        $registers->withholding_agent=$request->get('withholding_agent');
        $registers->is_exporter=$request->get('is_exporter');
        $registers->accountant=$request->get('accountant');
        $registers->ruc_counter=$request->get('ruc_counter');
        $registers->exp_Surpluses=$request->get('exp_Surpluses');
        $registers->social_media=$request->get('social_media');
        $registers->disability=$request->get('disability');
        $registers->percentage=$request->get('percentage');
        $registers->f_constitution=$request->get('f_constitution');
        $registers->n=$request->get('n');
        $registers->resol_n=$request->get('resol_n');
        $registers->code_n=$request->get('code_n');



        $registers->save();

        return  redirect('/register');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $register = Register::find($id);
        //dd($register);
        return view('register.show',compact('register'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $register =Register::find($id);
        //return view('register.edit')->with('register',$register);
        return view('register.edit')->with(compact('register'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $register =Register::find($id);

        $register->person_type=$request->get('person_type');
        $register->business_name=$request->get('business_name');
        $register->tradename=$request->get('tradename');
        $register->id_ruc=$request->get('id_ruc');
        $register->telephone=$request->get('telephone');
        $register->mobile=$request->get('mobile');
        $register->province=$request->get('province');
        $register->city=$request->get('city');
        $register->direction=$request->get('direction');
        $register->main_email=$request->get('main_email');
        $register->email_notificaions=$request->get('email_notificaions');
        $register->web_site=$request->get('web_site');
        $register->comments=$request->get('comments');

        $register->legal_representative=$request->get('legal_representative');
        $register->id_legal_representative=$request->get('id_legal_representative');
        $register->keep_accounting=$request->get('keep_accounting');
        $register->general_regime=$request->get('general_regime');
        $register->microenterprise=$request->get('microenterprise');
        $register->special_taxpayer=$request->get('special_taxpayer');
        $register->withholding_agent=$request->get('withholding_agent');
        $register->is_exporter=$request->get('is_exporter');
        $register->accountant=$request->get('accountant');
        $register->ruc_counter=$request->get('ruc_counter');
        $register->exp_Surpluses=$request->get('exp_Surpluses');
        $register->social_media=$request->get('social_media');
        $register->disability=$request->get('disability');
        $register->percentage=$request->get('percentage');
        $register->f_constitution=$request->get('f_constitution');
        $register->n=$request->get('n');
        $register->resol_n=$request->get('resol_n');
        $register->code_n=$request->get('code_n');



        $register->save();

        return  redirect('/register');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $register = Register::where('id',$id)->first();
        $register->delete();

        return redirect('/register')->with('success', 'Stock has been deleted Successfully');
    }

    public function byProvince($id)
    {
        return Register::where('id_registers','=',$id)->get();
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Client;
use App\Models\Institution;
use App\Models\Province;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class InstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $institutions=Institution::all();
        $provinces=Province::all();
        $cities=City::all();
        $now=Carbon::now();
        return view('institution.index')->with(compact('registers',$registers,
            'now','provinces',$provinces,'cities',$cities,'institutions',$institutions));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $institutions = new Institution();


        $institutions->person_type=$request->get('person_type');
        $institutions->business_name=$request->get('business_name');
        $institutions->tradename=$request->get('tradename');
        $institutions->id_ruc=$request->get('id_ruc');
        $institutions->telephone=$request->get('telephone');
        $institutions->mobile=$request->get('mobile');
        $institutions->province=$request->get('province');
        $institutions->city=$request->get('city');
        $institutions->direction=$request->get('direction');
        $institutions->main_email=$request->get('main_email');
        $institutions->email_notificaions=$request->get('email_notificaions');
        $institutions->web_site=$request->get('web_site');
        $institutions->comments=$request->get('comments');

        $institutions->legal_representative=$request->get('legal_representative');
        $institutions->id_legal_representative=$request->get('id_legal_representative');
        $institutions->keep_accounting=$request->get('keep_accounting');
        $institutions->general_regime=$request->get('general_regime');
        $institutions->microenterprise=$request->get('microenterprise');
        $institutions->special_taxpayer=$request->get('special_taxpayer');
        $institutions->withholding_agent=$request->get('withholding_agent');
        $institutions->is_exporter=$request->get('is_exporter');
        $institutions->accountant=$request->get('accountant');
        $institutions->ruc_counter=$request->get('ruc_counter');
        $institutions->exp_Surpluses=$request->get('exp_Surpluses');
        $institutions->social_media=$request->get('social_media');
        $institutions->disability=$request->get('disability');
        $institutions->percentage=$request->get('percentage');
        $institutions->f_constitution=$request->get('f_constitution');
        $institutions->n=$request->get('n');
        $institutions->resol_n=$request->get('resol_n');
        $institutions->code_n=$request->get('code_n');



        $institutions->save();

        return  redirect('/institution');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\AssignmentActivity;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AssignmentActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $assignment_activities=AssignmentActivity::all();
        $now=Carbon::now();
        return view('assignment_activity.index')->with(compact('registers',$registers,
            'now','assignment_activities',$assignment_activities));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $assignment_activities= new AssignmentActivity();

        $assignment_activities->activity_type=$request->get('activity_type');
        $assignment_activities->periodicity=$request->get('periodicity');
        $assignment_activities->service=$request->get('service');
        $assignment_activities->responsable=$request->get('responsable');
        $assignment_activities->activity=$request->get('activity');
        $assignment_activities->comments=$request->get('comments');
        $assignment_activities->start_date=$request->get('start_date');
        $assignment_activities->finish_date=$request->get('finish_date');
        $assignment_activities->priority=$request->get('priority');
        $assignment_activities->order=$request->get('order');
        $assignment_activities->estimated_time=$request->get('estimated_time');
        $assignment_activities->condition=$request->get('condition');
        $assignment_activities->advance=$request->get('advance');

        $assignment_activities->save();

        return  redirect('/assignment_activity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

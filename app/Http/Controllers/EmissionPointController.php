<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\EmissionPoint;
use App\Models\Register;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class EmissionPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {

        $branchId = $request->branchId;
        $emissionPoints=EmissionPoint::where('branches_id','=',$branchId )->get();
        return view('branch_offices.index')->with(compact('emissionPoints' ,'branchId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $emissionPoints = new EmissionPoint();

        $emissionPoints->location=$request->get('location');
        $emissionPoints->issuance_point=$request->get('issuance_point');
        $emissionPoints->emission_type=$request->get('emission_type');
        $emissionPoints->invoices=$request->get('invoices');
        $emissionPoints->withholdings=$request->get('withholdings');
        $emissionPoints->credit_notes=$request->get('credit_notes');
        $emissionPoints->debit_notes=$request->get('debit_notes');
        $emissionPoints->reference_guides=$request->get('reference_guides');
        $emissionPoints->purchase_liquidations=$request->get('purchase_liquidations');
        $emissionPoints->purchase_liquidations=$request->get('purchase_liquidations');

        $emissionPoints->save();

        return  redirect('/emission_point');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

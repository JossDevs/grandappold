<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Province;
use App\Models\Register;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $provinces=Province::all();
        $suppliers=Supplier::all();
        $cities=City::all();
        $now=Carbon::now();
        return view('supplier.index')->with(compact('registers',$registers,
            'now','provinces',$provinces,'cities',$cities,'suppliers',$suppliers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $suppliers = new Supplier();


        $suppliers->person_type=$request->get('person_type');
        $suppliers->business_name=$request->get('business_name');
        $suppliers->tradename=$request->get('tradename');
        $suppliers->id_ruc=$request->get('id_ruc');
        $suppliers->telephone=$request->get('telephone');
        $suppliers->mobile=$request->get('mobile');
        $suppliers->province=$request->get('province');
        $suppliers->city=$request->get('city');
        $suppliers->direction=$request->get('direction');
        $suppliers->main_email=$request->get('main_email');
        $suppliers->email_notificaions=$request->get('email_notificaions');
        $suppliers->web_site=$request->get('web_site');
        $suppliers->comments=$request->get('comments');

        $suppliers->legal_representative=$request->get('legal_representative');
        $suppliers->id_legal_representative=$request->get('id_legal_representative');
        $suppliers->keep_accounting=$request->get('keep_accounting');
        $suppliers->general_regime=$request->get('general_regime');
        $suppliers->microenterprise=$request->get('microenterprise');
        $suppliers->special_taxpayer=$request->get('special_taxpayer');
        $suppliers->withholding_agent=$request->get('withholding_agent');
        $suppliers->is_exporter=$request->get('is_exporter');
        $suppliers->accountant=$request->get('accountant');
        $suppliers->ruc_counter=$request->get('ruc_counter');
        $suppliers->exp_Surpluses=$request->get('exp_Surpluses');
        $suppliers->social_media=$request->get('social_media');
        $suppliers->disability=$request->get('disability');
        $suppliers->percentage=$request->get('percentage');
        $suppliers->f_constitution=$request->get('f_constitution');
        $suppliers->n=$request->get('n');
        $suppliers->resol_n=$request->get('resol_n');
        $suppliers->code_n=$request->get('code_n');



        $suppliers->save();

        return  redirect('/supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

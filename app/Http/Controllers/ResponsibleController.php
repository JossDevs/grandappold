<?php

namespace App\Http\Controllers;

use App\Models\Register;
use App\Models\Responsible;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ResponsibleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $responsibles=Responsible::all();
        $now=Carbon::now();
        return view('responsible.index')->with(compact('registers',$registers,'now','responsibles',$responsibles));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $responsibles= new Responsible();

        $responsibles->type=$request->get('type');
        $responsibles->name=$request->get('name');
        $responsibles->short_name=$request->get('short_name');
        $responsibles->comments=$request->get('comments');
        $responsibles->status=$request->get('status');
        $responsibles->start_date=$request->get('start_date');
        $responsibles->finish_date=$request->get('finish_date');


        if($request->hasFile('members')){
            $file=$request->file('members');
            $destinationPath='images/featureds/';
            $filename=time() . '_' . $file->getClientOriginalName();
            $uploadSuccess=$request->file('members')->move($destinationPath,$filename);
            $responsibles->members=$destinationPath . $filename;
        }



        $responsibles->save();

        return  redirect('/responsible');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

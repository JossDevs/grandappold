<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Financial;
use App\Models\FinancialEntity;
use App\Models\Province;
use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FinancialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers=Register::all();
        $financialEntities =FinancialEntity::all();
        $now=Carbon::now();
        $financials =Financial::all();
        return view('financial.index')->with(compact('registers',$registers,
            'now','financialEntities',$financialEntities,'financials',$financials));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $financials= new Financial();

        $financials->account_category=$request->get('account_category');
        $financials->financial_entity=$request->get('financial_entity');
        $financials->account_type=$request->get('account_type');
        $financials->account_card_number=$request->get('account_card_number');
        $financials->card_name=$request->get('card_name');
        $financials->card_quota=$request->get('card_quota');
        $financials->accounting_account=$request->get('accounting_account');
        $financials->registers_id=$request->get('registers_id');

        $financials->save();

        return  redirect('/financial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserClientsController;


 route::get('admin',[HomeController::class,'index']);

 route::get('userClients',[UserClientsController::class,'index']);

 //route::resource('AdminClients',\App\Http\Controllers\AdminClientsController::class);

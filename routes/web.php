<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserClientsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::resource('register',App\Http\Controllers\RegisterController::class);

//Route::get('register/{id}', 'App\Http\Controllers\RegisterController@byProvince');

Route::resource('branch',\App\Http\Controllers\BranchController::class);


Route::resource('product',\App\Http\Controllers\ProductController::class);

Route::resource('client',\App\Http\Controllers\ClientController::class);

Route::resource('supplier',\App\Http\Controllers\SupplierController::class);

Route::resource('financial',\App\Http\Controllers\FinancialController::class);

Route::resource('institution', \App\Http\Controllers\InstitutionController::class);

Route::resource('responsible',\App\Http\Controllers\ResponsibleController::class);

Route::resource('assignment_service',\App\Http\Controllers\AssignmentServicesController::class);

Route::resource('assignment_activity',\App\Http\Controllers\AssignmentActivitiesController::class);

Route::resource('emission_point',\App\Http\Controllers\EmissionPointController::class);



<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_activities', function (Blueprint $table) {
            $table->id('id_assignment_activities');

            $table->String('activity_type',50);
            $table->String('periodicity',50);
            $table->String('service',50);
            $table->String('responsable',50);
            $table->String('activity',50);
            $table->String('comments',200);

            $table->date('start_date');
            $table->date('finish_date');
            $table->String('priority',50);
            $table->String('order',50);
            $table->String('estimated_time',50);
            $table->String('condition',50);
            $table->String('advance',50);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_activities');
    }
}

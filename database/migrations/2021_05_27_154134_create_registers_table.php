<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->id();
            $table->String('person_type',50);
            $table->String('business_name',50);
            $table->String('tradename',50);
            $table->Integer('id_ruc');
            $table->Integer('telephone');
            $table->Integer('mobile');
            $table->String('province',50);
            $table->String('city',20);
            $table->String('direction',50);
            $table->String('main_email',50);
            $table->String('email_notificaions',50);
            $table->String('web_site',20);
            $table->String('comments',100)->nullable();

            $table->String('legal_representative',50);
            $table->Integer('id_legal_representative');
            $table->Boolean('keep_accounting');
            $table->Boolean('general_regime');
            $table->Boolean('microenterprise');
            $table->Boolean('special_taxpayer');
            $table->Boolean('withholding_agent');
            $table->Boolean('is_exporter');
            $table->String('accountant',50);
            $table->Integer('ruc_counter');
            $table->Integer('exp_Surpluses');
            $table->String('social_media',50);
            $table->String('disability',50);
            $table->Integer('percentage');
            $table->Date('f_constitution');
            $table->Integer('n');
            $table->Integer('resol_n');
            $table->Integer('code_n');





            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_services', function (Blueprint $table) {
            $table->id('id_assignment_services');
            $table->String('service',50);
            $table->String('adviser',50);
            $table->String('rate_type',50);
            $table->String('rate_period',50);
            $table->String('comments',50);
            $table->Double('subtotal');
            $table->Double('iva');
            $table->Double('total');
            $table->String('status',50);
            $table->date('start_date');
            $table->date('finish_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_services');
    }
}

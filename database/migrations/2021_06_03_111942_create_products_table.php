<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id('id_products');
            $table->String('product_type',50);
            $table->String('category',100);
            $table->Integer('main_code');
            $table->Integer('auxiliary_code');
            $table->String('product_name',50);
            $table->String('brand',50);
            $table->String('model',50);
            $table->String('unit_measure',50);
            $table->Double('subtotal');
            $table->Double('iva');
            $table->Double('ice');
            $table->String('comments',50);
            $table->date('start_date');
            $table->date('finish_date');
            $table->String('status',50);
            $table->Double('average');
            $table->String('photo',200);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

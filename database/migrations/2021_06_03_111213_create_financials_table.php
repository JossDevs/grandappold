<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financials', function (Blueprint $table) {
            $table->id('id_financials');
            $table->String('account_category',100);
            $table->String('financial_entity',100);
            $table->String('account_type',100);
            $table->Integer('account_card_number');
            $table->String('card_name',50);
            $table->String('card_quota',50);
            $table->String('accounting_account',50);
            $table->unsignedBigInteger('registers_id');

            $table->foreign('registers_id')
                ->references('id')
                ->on('registers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financials');
    }
}

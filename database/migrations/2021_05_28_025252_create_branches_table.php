<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id('id_branches');
            $table->String('type',50);
            $table->String('branch',50);
            $table->String('name',50);
            $table->String('logo',200);
            $table->String('firm',200);
            $table->String('country',50);
            $table->String('province',50);
            $table->String('city',20);
            $table->Integer('telephone');
            $table->Integer('phone');
            $table->String('direction',50);
            $table->String('email',50);
            $table->String('website',50);
            $table->String('social_media',50);
            $table->String('activity',50);
            $table->date('start_date');
            $table->date('final_date');

            $table->unsignedBigInteger('registers_id');

            $table->foreign('registers_id')
                ->references('id')
                ->on('registers')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmissionPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emission_points', function (Blueprint $table) {
            $table->id('id_emission_points');

            $table->String('location',50);
            $table->String('issuance_point',50);
            $table->String('emission_type',50);
            $table->String('invoices',50);
            $table->String('withholdings',50);
            $table->String('credit_notes',200);
            $table->String('debit_notes',50);
            $table->String('reference_guides',50);
            $table->String('purchase_liquidations',50);
            $table->unsignedBigInteger('branches_id');

            $table->foreign('branches_id')
                ->references('id_branches')
                ->on('branches')
                ->onDelete('cascade');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emission_points');
    }
}

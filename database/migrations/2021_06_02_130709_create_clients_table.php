<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id('id_clients');
            $table->String('person_type',50);
            $table->String('business_name',50);
            $table->String('tradename',50);
            $table->Integer('id_ruc');
            $table->Integer('telephone');
            $table->Integer('mobile');
            $table->String('province',50);
            $table->String('city',20);
            $table->String('direction',50);
            $table->String('main_email',50);
            $table->String('email_notificaions',50);
            $table->String('web_site',20);
            $table->String('comments',100);

            $table->String('legal_representative',50);
            $table->Integer('id_legal_representative');
            $table->String('keep_accounting',50)->nullable();
            $table->String('general_regime',50)->nullable();
            $table->String('microenterprise',50)->nullable();
            $table->String('special_taxpayer',50)->nullable();
            $table->String('withholding_agent',50)->nullable();
            $table->String('is_exporter',50)->nullable();
            $table->String('accountant',50);
            $table->Integer('ruc_counter');
            $table->Integer('exp_Surpluses');
            $table->String('social_media',50);
            $table->String('disability',50);
            $table->Integer('percentage');
            $table->Date('f_constitution');
            $table->Integer('n');
            $table->Integer('resol_n');
            $table->Integer('code_n');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
